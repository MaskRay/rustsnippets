fn brent<T: Clone+Eq, F>(f: F, start: T) -> (usize, usize) where F: Fn(T) -> T {
  let mut pw = 1;
  let mut l = 1;
  let mut x = start.clone();
  let mut y = f(start.clone());
  while x != y {
    if l == pw {
      x = y.clone();
      pw *= 2;
      l = 0;
    }
    y = f(y);
    l += 1;
  }
  x = start.clone();
  y = start;
  for _ in 0..l {
    y = f(y);
  }
  let mut mu = 0;
  while x != y {
    x = f(x);
    y = f(y);
    mu += 1;
  }
  (mu, l)
}

#[cfg(test)]
mod tests {
  use super::brent;
  #[test]
  fn test() {
    let f0 = |x| { [1,2,3,4,5,2][x] };
    assert_eq!(brent(f0, 0), (2, 4));
    let f1 = |x| { [0][x] };
    assert_eq!(brent(f1, 0), (0, 1));
  }
}
