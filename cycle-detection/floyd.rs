fn floyd<T: Clone+Eq, F>(f: F, start: T) -> (usize, usize) where F: Fn(T) -> T {
  let mut x = start.clone();
  let mut y = start.clone();
  loop {
    x = f(x);
    y = f(f(y));
    if x == y { break; }
  }
  let mut mu = 0;
  x = start;
  while x != y {
    x = f(x);
    y = f(y);
    mu += 1;
  }
  let mut l = 0;
  loop {
    y = f(y);
    l += 1;
    if x == y { break; }
  }
  (mu, l)
}

#[cfg(test)]
mod tests {
  use super::floyd;
  #[test]
  fn test() {
    let f0 = |x| { [1,2,3,4,5,2][x] };
    assert_eq!(floyd(f0, 0), (2, 4));
    let f1 = |x| { [0][x] };
    assert_eq!(floyd(f1, 0), (0, 1));
  }
}
