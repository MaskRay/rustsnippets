#![feature(unsafe_destructor)]

use std::ptr;
use std::mem;
use std::fmt;

struct List<T> {
  head: Link<T>,
  tail: *mut Node<T>,
}

struct Node<T> {
  next: Link<T>,
  prev: *mut Node<T>,
  value: T,
}

type Link<T> = Option<Box<Node<T>>>;

impl<T> Node<T> {
  fn new(v: T) -> Node<T> {
    Node{value: v, next: None, prev: ptr::null_mut()}
  }
}

fn link_with_prev<T>(mut next: Box<Node<T>>, prev: *mut Node<T>) -> Link<T> {
  next.prev = prev;
  Some(next)
}

impl<T> List<T> {
  fn new() -> List<T> {
    List{head: None, tail: ptr::null_mut()}
  }

  fn push_front_node(&mut self, mut x: Box<Node<T>>) {
    match self.head {
      None => {
        self.tail = &mut *x;
        self.head = Some(x);
      }
      Some(ref mut head) => {
        head.prev = &mut *x;
        mem::swap(head, &mut x);
        head.next = Some(x);
      }
    }
  }

  fn push_front(&mut self, x: T) {
    self.push_front_node(Box::new(Node::new(x)));
  }

  fn pop_front_node(&mut self) -> Option<Box<Node<T>>> {
    self.head.take().map(|mut x| {
      match x.next.take() {
        None =>
          self.tail = ptr::null_mut(),
        Some(y) =>
          self.head = link_with_prev(y, ptr::null_mut())
      }
      x
    })
  }

  fn pop_front(&mut self) -> Option<T> {
    self.pop_front_node().map(|x| (*x).value)
  }

  fn push_back_node(&mut self, mut x: Box<Node<T>>) {
    if self.tail.is_null() {
      self.tail = &mut *x;
      self.head = Some(x);
    } else {
      let y = self.tail;
      self.tail = &mut *x;
      unsafe {
        x.prev = &mut *y;
        (*y).next = Some(x);
      }
    }
  }

  fn push_back(&mut self, x: T) {
    self.push_back_node(Box::new(Node::new(x)));
  }

  fn pop_back_node(&mut self) -> Option<Box<Node<T>>> {
    let mut tail = self.tail;
    if tail.is_null() {
      None
    } else {
      unsafe {
        self.tail = (*tail).prev;
        if self.tail.is_null() {
          self.head.take()
        } else {
          (*self.tail).next.take()
        }
      }
    }
  }

  fn pop_back(&mut self) -> Option<T> {
    self.pop_back_node().map(|x| (*x).value)
  }
}

struct E {
  v: isize,
}

impl fmt::Debug for E {
  fn fmt(&self, _f: &mut fmt::Formatter) -> Result<(),fmt::Error> {
    //print!("{}", 1);
    Ok(())
  }
}

impl Drop for E {
  fn drop(&mut self) {
    println!("1");
  }
}

// default destructor (dropping head) leads to prevent stack overflow
#[unsafe_destructor]
impl<T> Drop for List<T> {
  fn drop(&mut self) {
    let mut tail = self.tail;
    loop {
      if tail.is_null() {
        break
      }
      unsafe {
        (*tail).next.take();
        tail = (*tail).prev;
      }
    }
    self.head.take();
  }
}

fn main() {
  let mut a = List::new();
  for i in 0..100000 {
    a.push_back(E{v:i});
  }
  //println!("{:?}", a.pop_back().unwrap());
  //println!("{:?}", a.pop_back().unwrap());
  //println!("{:?}", a.pop_back().unwrap());
  //println!("{}", a.pop_front().unwrap());
  //println!("{}", a.pop_back().unwrap());
  //println!("{}", a.pop_front().unwrap());
  //println!("{}", a.pop_front().unwrap());
}
