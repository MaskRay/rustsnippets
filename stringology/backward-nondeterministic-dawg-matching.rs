fn BNDM<'a, 'b>(t: &'a str, s: &'b str) {
  let t = t.as_bytes();
  let s = s.as_bytes();
  let n = t.len();
  let m = s.len();
  if n < m {
    return;
  }
  let mut b = vec![];
  b.resize(256, 0us);
  for i in 0..m {
    b[s[i] as usize] |= 1 << i;
  }
  let mut i = 0;
  while i <= n-m {
    let mut j = m;
    let mut d = -1;
    let mut shift = m;
    while j > 0 && d > 0 {
      j -= 1;
      d &= b[t[i+j] as usize];
      if d > 0 {
        if j > 0 {
          shift = j;
        } else {
          println!("{}", i);
        }
      }
      d >>= 1;
    }
    i += shift;
  }
}

fn main() {
  BNDM("ababcababcab", "ab");
}
