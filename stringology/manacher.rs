// longest odd palindrome substring for each position in a string

use std::cmp::{max, min};

fn manacher<'a>(s: &'a str) -> Vec<usize> {
  let s = s.as_bytes();
  let mut z = Vec::with_capacity(s.len());
  let mut f = 0;
  let mut g = 0;
  z.push(1);
  for i in 1..s.len() {
    if i < g && g-i != z[2*f-i] {
      let x = min(z[2*f-i], g-i);
      z.push(x);
    } else {
      f = i;
      g = max(g, i);
      while g < s.len() && 2*f >= g && s[g] == s[2*f-g] {
        g += 1;
      }
      z.push(g-f);
    }
  }
  z
}

fn main() {
  let s = "abababacbabcba";
  let z = manacher(s);
  for i in 0..s.len() {
    print!("{} ", s.as_bytes()[i] as char);
  }
  println!("");
  for i in 0..z.len() {
    print!("{} ", z[i]);
  }
  println!("");
}
