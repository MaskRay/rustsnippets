fn knuth_morris_pratt<'a,'b>(t: &'a str, s: &'b str) -> Option<usize> {
  let t = t.as_bytes();
  let s = s.as_bytes();
  let mut pi = vec![];
  let mut j = 0;
  pi.push(0);
  for i in 0..s.len() {
    while j > 0 && s[i] != s[j-1] {
      j = pi[j-1];
    }
    j += 1;
    if i+1 < s.len() && s[i+1] == s[j-1] {
      let x = pi[j-1];
      pi.push(x);
    } else {
      pi.push(j);
    }
    // invariant: pi[i] = max{x : s[0..x-2] == s[i-x+1..i-1] && s[x-1] != s[i]}
  }
  j = 1;
  for i in 0..t.len() {
    while j > 0 && t[i] != s[j-1] {
      j = pi[j-1];
    }
    if j == s.len() {
      return Some(i-j+1);
    }
    j += 1;
  }
  None
}

#[test]
fn test() {
  assert_eq!(Some(2), knuth_morris_pratt("dababbababbabba", "babbababbabba"));
}
