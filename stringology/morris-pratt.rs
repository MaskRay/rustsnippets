fn morris_pratt<'a,'b>(t: &'a str, s: &'b str) -> Option<usize> {
  let t = t.as_bytes();
  let s = s.as_bytes();
  let mut border = vec![];
  let mut j = 0;
  border.push(0);
  for i in 1..s.len() {
    while j > 0 && s[i] != s[j] {
      j = border[j-1];
    }
    if s[i] == s[j] {
      j += 1;
    }
    border.push(j);
  }
  j = 0;
  for i in 0..t.len() {
    while j > 0 && t[i] != s[j] {
      j = border[j-1];
    }
    if t[i] == s[j] {
      j += 1;
      if j == s.len() {
        return Some(i-s.len()+1);
      }
    }
  }
  None
}

#[test]
fn test() {
  assert_eq!(Some(2), morris_pratt("ababcd", "abc"));
  assert_eq!(None, morris_pratt("ababd", "abc"));
}
