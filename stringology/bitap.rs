use std::usize;

/// s.len() <= 8 * sizeof::<usize>()
fn bitap<'a,'b>(t: &'a str, s: &'b str) -> Option<usize> {
  if s.len() == 0 {
    return Some(0);
  }
  let t = t.as_bytes();
  let s = s.as_bytes();
  let mut m = Vec::new();
  m.resize(256, usize::MAX);
  for i in 0..s.len() {
    m[s[i] as usize] &= ! (1 << i);
  }
  let mut r = ! 1;
  for i in 0..t.len() {
    r |= m[t[i] as usize];
    // invariant: r[j] == 0 <=> s[0,j] == t[i-j,i]
    if (r & (1 << s.len()-1)) == 0 {
      return Some(i-s.len()+1);
    }
    r <<= 1;
  }
  None
}

/// Return the ending position of the first match with up to `k` errors.
///
/// s.len() < 8 * sizeof::<usize>()
fn bitap_fuzzy<'a,'b>(t: &'a str, s: &'b str, k: usize) -> Option<usize> {
  if s.len() <= k {
    return Some(0);
  }
  let t = t.as_bytes();
  let s = s.as_bytes();
  let mut m = Vec::new();
  m.resize(256, usize::MAX);
  for i in 0..s.len() {
    m[s[i] as usize] &= ! (1 << i);
  }
  let mut r = Vec::new();
  for i in 0..k+1 {
    r.push(- (1 << i) - 1);
  }
  for i in 0..t.len() {
    let mut old = r[0];
    r[0] = (r[0] | m[t[i] as usize]) << 1;
    // invariant: r[j][x] == 0 <-> dist(s[0,x-1], some factor of t ending in i) <= j
    for j in 1..k+1 {
      let old2 = r[j];
      r[j] = (r[j] | m[t[i] as usize]) << 1;
      // dist(s[0,x-2], some factor of t ending in i-1) <= j-1
      // -> substitute
      r[j] &= old << 1;
      // dist(s[0,x-1], some factor of t ending in i-1) <= j-1
      // -> delete t[i]
      r[j] &= old;
      // dist(s[0,x-2], some factor of t ending in i) <= j-1
      // -> insert s[x-1]
      r[j] &= r[j-1] << 1;
      old = old2;
    }
    if r[k] & (1 << s.len()) == 0 {
      return Some(i);
    }
  }
  None
}

#[test]
fn test() {
  assert_eq!(bitap("hello", "ll"), Some(2));
  assert_eq!(bitap_fuzzy("hello", "ll", 0), Some(3));
  assert_eq!(bitap_fuzzy("hello", "ahello", 1), Some(4));
}
