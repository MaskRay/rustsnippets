use std::cmp::{max, min};

fn gusfield_z<'a>(s: &'a str) -> Vec<usize> {
  let s = s.as_bytes();
  let mut z = Vec::with_capacity(s.len());
  let mut f = 0;
  let mut g = 0;
  z.push(s.len());
  for i in 1..s.len() {
    if i < g && g-i != z[i-f] {
      let x = min(z[i-f], g-i);
      z.push(x);
    } else {
      f = i;
      g = max(g, i);
      while g < s.len() && s[g] == s[g-f] {
        g += 1;
      }
      z.push(g-f);
    }
  }
  z
}

fn main() {
  let z = gusfield_z("ababcababcac");
  for i in 0..z.len() {
    print!("{} ", z[i]);
  }
}
