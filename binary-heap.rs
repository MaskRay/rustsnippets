use std::ptr;
use std::cmp::Ordering;
use std::fmt;

struct BinaryHeap<T> {
  a: Vec<T>,
}

struct E {
  v: isize,
}

impl Drop for E {
  fn drop(&mut self) {
    println!("drop {}", self.v);
  }
}

impl PartialEq for E {
  fn eq(&self, o: &E) -> bool {
    self.v == o.v
  }
}

impl Eq for E {}

impl PartialOrd for E {
  fn partial_cmp(&self, o: &E) -> Option<Ordering> {
    self.v.partial_cmp(&o.v)
  }
}

impl Ord for E {
  fn cmp(&self, o: &E) -> Ordering {
    self.v.cmp(&o.v)
  }
}

impl fmt::Debug for E {
  fn fmt(&self, _f: &mut fmt::Formatter) -> Result<(),fmt::Error> {
    print!("{}", self.v);
    Ok(())
  }
}

impl<T: Ord> BinaryHeap<T> {
  fn new() -> BinaryHeap<T> {
    BinaryHeap{a: vec![]}
  }

  fn len(&self) -> usize {
    self.a.len()
  }

  fn is_empty(&self) -> bool {
    self.a.is_empty()
  }

  fn top(&self) -> &T {
    &self.a[0]
  }

  fn push(&mut self, elem: T) {
    let x = self.len();
    self.a.push(elem);
    self.up(x);
  }

  fn pop(&mut self) -> T {
    let mut elem = self.a.pop().unwrap();
    if ! self.is_empty() {
      unsafe {
        ptr::swap(&mut elem, &mut self.a[0]);
      }
      self.down(0);
    }
    elem
  }

  fn from_vec(a: Vec<T>) -> BinaryHeap<T> {
    let mut heap = BinaryHeap{a: a};
    let mut x = heap.len()/2;
    while x > 0 {
      x -= 1;
      heap.down(x);
    }
    heap
  }

  fn up(&mut self, mut x: usize) {
    unsafe {
      let elem = ptr::read(&self.a[x]);
      while x > 0 {
        let y = (x-1)/2;
        if ! (elem < self.a[y]) {
          break;
        }
        let yy = ptr::read(&self.a[y]);
        ptr::write(&mut self.a[x], yy);
        x = y;
      }
      ptr::write(&mut self.a[x], elem);
    }
  }

  fn down(&mut self, mut x: usize) {
    unsafe {
      let n = self.len();
      let elem = ptr::read(&self.a[x]);
      loop {
        let mut y = 2*x+1;
        if y >= n {
          break;
        }
        if y+1 < n && self.a[y+1] < self.a[y] {
          y += 1;
        }
        if ! (self.a[y] < elem) {
          break;
        }
        let yy = ptr::read(&self.a[y]);
        ptr::write(&mut self.a[x], yy);
        x = y;
      }
      ptr::write(&mut self.a[x], elem);
    }
  }
}

fn main() {
  let a = vec![E{v:6},E{v:1},E{v:3},E{v:2},E{v:5},E{v:4}];
  let mut h = BinaryHeap::from_vec(a);
  h.push(E{v:0});
  h.push(E{v:4});
  while ! h.is_empty() {
    let x = h.pop();
    println!("{:?}", x);
  }
}
