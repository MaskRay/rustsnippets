#![crate_type = "lib"]
#![feature(core)]
use std::intrinsics;

extern crate bit;
extern crate darray;
use bit::BitVec;
use darray::{DArray1, DArray0};

/// limit: n <= 2**30-2 (<-- highs.len() < 2**31)

pub struct EliasFano {
  bound: usize,
  l: usize,
  highs: BitVec,
  highs_d1: DArray1,
  highs_d0: DArray0,
  lows: BitVec,
}

#[derive(Clone)]
pub struct EliasFanoBuilder {
  bound: usize,
  n: usize,
  pos: usize,
  l: usize,
  highs: BitVec,
  lows: BitVec,
}

impl EliasFanoBuilder {
  pub fn new(n: usize, bound: usize) -> EliasFanoBuilder {
    let l =
      if n > 0 && bound/n > 0 {
        unsafe{ 31-intrinsics::ctlz32((bound/n) as u32) as usize }
      } else {
        0
      };
    EliasFanoBuilder {
      bound: bound,
      n: n,
      pos: 0,
      l: l,
      highs: BitVec::from_elem((bound>>l)+n+1,false),
      lows: BitVec::new(),
    }
  }

  pub fn push(&mut self, x: usize) {
    if self.l > 0 {
      self.lows.append_bits(self.l, (x & (1<<self.l)-1) as u64);
    }
    self.highs.set((x>>self.l)+self.pos, true);
    self.pos += 1;
  }
}

impl EliasFano {
  pub fn new(b: EliasFanoBuilder) -> EliasFano {
    //assert_eq!(b.n, b.pos);
    let highs_d1 = DArray1::new(&b.highs);
    let highs_d0 = DArray0::new(&b.highs);
    EliasFano {
      bound: b.bound,
      l: b.l,
      highs: b.highs,
      highs_d1: highs_d1,
      highs_d0: highs_d0,
      lows: b.lows,
    }
  }

  pub fn at(&self, idx: u32) -> usize {
    ((self.highs_d1.select(&self.highs, idx) - idx) as usize) << self.l |
      (if self.l > 0 { self.lows.get_bits(idx as usize*self.l, self.l) as usize } else { 0 })
  }

  pub fn rank(&self, idx: usize) -> usize {
    let hi = idx>>self.l;
    let lo = (idx & (1<<self.l)-1) as u64;
    let mut i = self.highs_d0.select(&self.highs, hi as u32) as usize;
    let mut r = i-hi; // number of elements in highs <= hi
    while i > 0 && self.highs[i-1] && (if self.l > 0 { self.lows.get_bits((r-1)*self.l, self.l) } else { 0 }) >= lo {
      i -= 1;
      r -= 1;
    }
    r
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn test() {
    let a = &[0,0,1,3,3,3,5,6,510,512,1020,1021,1023];
    let mut b = EliasFanoBuilder::new(13, 1023);
    for &x in a {
      b.push(x);
    }
    let ef = EliasFano::new(b);
    for i in 0..13 {
      assert_eq!(ef.at(i as u32), a[i]);
    }
    assert_eq!(ef.rank(0), 0);
    assert_eq!(ef.rank(1), 2);
    assert_eq!(ef.rank(3), 3);
    assert_eq!(ef.rank(4), 6);
    assert_eq!(ef.rank(512), 9);
    assert_eq!(ef.rank(513), 10);
  }
}
