#![crate_type = "lib"]
use std::iter::*;
use std::cmp::min;

extern crate bit;
extern crate elias_fano;
use bit::BitVec;
use elias_fano::{EliasFano, EliasFanoBuilder};

fn lower_bound<T: Ord>(a: &Vec<T>, mut l: usize, mut h: usize, x: T) -> usize {
  while l < h {
    let m = l+(h-l)/2;
    if a[m] < x {
      l = m+1;
    } else {
      h = m;
    }
  }
  l
}

fn upper_bound<T: Ord>(a: &Vec<T>, mut l: usize, mut h: usize, x: T) -> usize {
  while l < h {
    let m = l+(h-l)/2;
    if a[m] <= x {
      l = m+1;
    } else {
      h = m;
    }
  }
  l
}

fn build_preflight(d: usize, l: usize, h: usize, cntl: &mut Vec<usize>) {
  if h-l == 1 { return; }
  if cntl.len() <= d { cntl.push(0); }
  let m = l+(h-l)/2;
  cntl[d] += m-l;
  build_preflight(d+1, l, m, cntl);
  build_preflight(d+1, m, h, cntl);
}

pub struct WaveletTree {
  n: usize,
  a: Vec<Vec<usize>>,
  sorted: Vec<usize>,
  ef1: Vec<EliasFano>,
  ef0: Vec<EliasFano>,
}

impl WaveletTree {
  pub fn new(a: Vec<usize>) -> WaveletTree {
    let n = a.len();
    let mut sorted = a.clone();
    sorted.sort();
    let mut ret = WaveletTree {
      n: n,
      a: vec![a, repeat(0).take(n).collect()],
      sorted: sorted,
      ef1: vec![],
      ef0: vec![],
    };
    let mut cntl = vec![];
    build_preflight(0, 0, n, &mut cntl);
    let mut efb1 = vec![];
    let mut efb0 = vec![];
    for d in 0..cntl.len() {
      efb1.push(EliasFanoBuilder::new(cntl[d], n-1));
      efb0.push(EliasFanoBuilder::new(n-cntl[d], n-1));
    }
    ret.build(0, 0, n, &mut efb1, &mut efb0);
    for efb in efb1.into_iter() {
      ret.ef1.push(EliasFano::new(efb));
    }
    for efb in efb0.into_iter() {
      ret.ef0.push(EliasFano::new(efb));
    }
    ret
  }

  fn build(&mut self, d: usize, l: usize, h: usize, efb1: &mut Vec<EliasFanoBuilder>, efb0: &mut Vec<EliasFanoBuilder>) {
    if h-l == 1 { return; }
    let m = l+(h-l)/2;
    let pivot = self.sorted[m-1];
    let mut eq = m-lower_bound(&self.sorted, l, h, pivot);
    let mut j0 = l;
    let mut j1 = m;
    for i in l..h {
      let x = self.a[d&1][i];
      if x < pivot || x == pivot && eq > 0 {
        if x == pivot { eq -= 1; }
        self.a[d+1&1][j0] = x;
        efb1[d].push(i);
        j0 += 1;
      } else {
        self.a[d+1&1][j1] = x;
        efb0[d].push(i);
        j1 += 1;
      }
    }
    self.build(d+1, l, m, efb1, efb0);
    self.build(d+1, m, h, efb1, efb0);
  }

  // position of the `k`-th occurrence of symbol `x`
  pub fn select(&self, x: usize, k: usize) -> usize {
    let r = lower_bound(&self.sorted, 0, self.n, x) + k;
    self.select_(0, 0, self.n, r)
  }

  fn select_(&self, d: usize, l: usize, h: usize, r: usize) -> usize {
    if l == h-1 {
      return l;
    }
    let m = l+(h-l)/2;
    if r < m {
      let j = self.select_(d+1, l, m, r) - l;
      self.ef1[d].at((self.ef1[d].rank(l)+j) as u32)
    } else {
      let j = self.select_(d+1, m, h, r) - m;
      self.ef0[d].at((self.ef0[d].rank(l)+j) as u32)
    }
  }

  // number of occurrences of symbol `x` in [0,i)
  // 0 <= i <= n
  pub fn rank(&self, x: usize, i: usize) -> usize {
    self.rank_(0, 0, self.n, x, i)
  }

  fn rank_(&self, d: usize, l: usize, h: usize, x: usize, i: usize) -> usize {
    if self.sorted[l] == self.sorted[h-1] {
      return if x == self.sorted[l] { i } else { 0 };
    }
    let m = l+(h-l)/2;
    let mut ret = 0;
    let j = self.ef1[d].rank(l+i)-self.ef1[d].rank(l);
    if x <= self.sorted[m-1] {
      ret += self.rank_(d+1, l, m, x, j);
    }
    if self.sorted[m] <= x {
      ret += self.rank_(d+1, m, h, x, i-j);
    }
    ret
  }

  // 0 <= ll < hh <= n
  // 0 <= k < hh-ll
  pub fn kth(&self, ll: usize, hh: usize, k: usize) -> usize {
    self.kth_(0, 0, self.n, ll, hh, k)
  }

  fn kth_(&self, d: usize, l: usize, h: usize, ll: usize, hh: usize, k: usize) -> usize {
    if h-l == 1 {
      return self.sorted[l];
    }
    let m = l+(h-l)/2;
    let rank_l = self.ef1[d].rank(l);
    let cl = self.ef1[d].rank(ll)-rank_l;
    let ch = self.ef1[d].rank(hh)-rank_l;
    if k < ch-cl {
      self.kth_(d+1, l, m, l+cl, l+ch, k)
    } else {
      self.kth_(d+1, m, h, m+(ll-l-cl), m+(hh-l-ch), k-(ch-cl))
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use std::cmp::min;
  #[test]
  fn test() {
    let a = vec![3,1,3,6,2,0,2,2];
    let wt = WaveletTree::new(a.clone());
    assert_eq!(wt.select(0, 0), 5);
    assert_eq!(wt.select(1, 0), 1);
    assert_eq!(wt.select(2, 0), 4);
    assert_eq!(wt.select(2, 1), 6);
    assert_eq!(wt.select(2, 2), 7);
    assert_eq!(wt.select(3, 0), 0);
    assert_eq!(wt.select(3, 1), 2);
    for i in 0..8 {
      for j in 0..a.len() {
        let mut c = 0;
        for k in 0..min(j, a.len()) {
          if a[k] == i { c += 1 }
        }
        assert_eq!(wt.rank(i, j), c);
      }
    }
    for i in 0..a.len() {
      let mut b = vec![];
      for j in i+1..a.len() {
        b.push(a[j-1]);
        b.sort();
        for k in 0..j-i {
          assert_eq!(wt.kth(i, j, k), b[k]);
        }
      }
    }
  }
}
