#![crate_type = "lib"]
use std::iter::repeat;
use std::ops::Index;

static TRUE: bool = true;
static FALSE: bool = false;

#[derive(Clone)]
pub struct BitVec {
  pub storage: Vec<u64>,
  nbits: usize
}

impl BitVec {
  pub fn new() -> BitVec {
    BitVec {
      storage: vec![],
      nbits: 0,
    }
  }
  // nbits <= usize::MAX-63
  pub fn from_elem(nbits: usize, bit: bool) -> BitVec {
    let nblocks = (nbits+63)/64;
    BitVec {
      storage: repeat(if bit {!0} else {0}).take(nblocks).collect(),
      nbits: nbits
    }
  }
  pub fn iter(&self) -> Iter {
    Iter { bit_set: self, idx: 0 }
  }
  pub fn len(&self) -> usize { self.nbits }
  pub fn set(&mut self, i: usize, bit: bool) {
    let w = i/64;
    let b = 1 << i%64;
    if bit {
      self.storage[w] |= b;
    } else {
      self.storage[w] &= ! b;
    }
  }
  pub fn get(&self, i: usize) -> bool {
    (self.storage[i/64] & (1 << i%64)) != 0
  }
  pub fn append_bits(&mut self, len: usize, bits: u64) {
    let pos_in_word = self.nbits%64;
    if pos_in_word == 0 {
      self.storage.push(bits);
    } else {
      let last = self.storage.len()-1;
      self.storage[last] |= bits << pos_in_word;
      if len > 64-pos_in_word {
        self.storage.push(bits >> 64-pos_in_word);
      }
    }
    self.nbits += len;
  }
  pub fn get_bits(&self, i: usize, len: usize) -> u64 {
    let w = i/64;
    let shift = i%64;
    let mask = (-1) >> 64-len;
    if shift+len <= 64 {
      self.storage[w] >> shift & mask
    } else {
      self.storage[w] >> shift |
        (self.storage[w+1] << 64-shift & mask)
    }
  }
}

impl Index<usize> for BitVec {
  type Output = bool;
  fn index(&self, i: &usize) -> &bool {
    if self.get(*i) {
      &TRUE
    } else {
      &FALSE
    }
  }
}

#[derive(Clone)]
pub struct Iter<'a> {
  bit_set: &'a BitVec,
  idx: usize,
}

impl<'a> Iterator for Iter<'a> {
  type Item = bool;
  fn next(&mut self) -> Option<bool> {
    if self.idx == self.bit_set.len() {
      None
    } else {
      self.idx += 1;
      Some(self.bit_set[self.idx-1])
    }
  }
  fn size_hint(&self) -> (usize, Option<usize>) {
    let rem = self.bit_set.len()-self.idx;
    (rem, Some(rem))
  }
}

#[cfg(test)]
mod tests {
  #[test]
  fn test() {
    let mut bs = super::BitVec::from_elem(100, false);
    bs.set(3, true);
    assert!(bs.get(3), true);
    bs.set(65, true);
    assert!(bs.get(65), true);
  }
}
