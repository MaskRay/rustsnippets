//#![crate_type = "lib"]
use std::iter::*;
use std::cmp::min;

fn lower_bound<T: Ord>(a: &Vec<T>, mut l: usize, mut h: usize, x: T) -> usize {
  while l < h {
    let m = l+(h-l)/2;
    if a[m] < x {
      l = m+1;
    } else {
      h = m;
    }
  }
  l
}

pub struct UncompressedWaveletTree {
  n: usize,
  a: Vec<Vec<usize>>,
  sorted: Vec<usize>,
  sel: Vec<Vec<usize>>,
  rnk: Vec<Vec<usize>>,
}

impl UncompressedWaveletTree {
  pub fn new(a: Vec<usize>) -> UncompressedWaveletTree {
    let n = a.len();
    let mut sorted = a.clone();
    sorted.sort();
    let mut ret = UncompressedWaveletTree {
      n: n,
      a: vec![a, repeat(0).take(n).collect()],
      sorted: sorted,
      sel: vec![],
      rnk: vec![],
    };
    ret.build(0, 0, n);
    ret
  }

  fn build(&mut self, d: usize, l: usize, h: usize) {
    if self.sel.len() <= d {
      self.sel.push(repeat(0).take(self.n).collect());
      self.rnk.push(repeat(0).take(self.n).collect());
    }
    if h-l == 1 { return; }
    let m = l+(h-l)/2;
    let pivot = self.sorted[m-1];
    let mut eq = m-lower_bound(&self.sorted, l, h, pivot);
    let mut j0 = l;
    let mut j1 = m;
    for i in l..h {
      let x = self.a[d&1][i];
      self.rnk[d][i] = j0-l;
      if x < pivot || x == pivot && eq > 0 {
        if x == pivot {
          eq -= 1;
        }
        self.a[d+1&1][j0] = x;
        self.sel[d][j0] = i;
        j0 += 1;
      } else {
        self.a[d+1&1][j1] = x;
        self.sel[d][j1] = i;
        j1 += 1;
      }
    }
    self.build(d+1, l, m);
    self.build(d+1, m, h);
  }

  // position of the `k`-th occurrence of symbol `x`
  pub fn select(&self, x: usize, k: usize) -> usize {
    let r = lower_bound(&self.sorted, 0, self.n, x) + k;
    self.select_(0, 0, self.n, r)
  }

  fn select_(&self, d: usize, l: usize, h: usize, r: usize) -> usize {
    if l == h-1 {
      return l;
    }
    let m = l+(h-l)/2;
    let j =
      if r < m { self.select_(d+1, l, m, r) }
      else { self.select_(d+1, m, h, r) };
    self.sel[d][j]
  }

  // number of occurrences of symbol `x` in [0,i)
  // 0 <= i
  pub fn rank(&self, x: usize, i: usize) -> usize {
    self.rank_(0, 0, self.n, x, i)
  }

  fn rank_(&self, d: usize, l: usize, h: usize, x: usize, i: usize) -> usize {
    if self.sorted[l] == self.sorted[h-1] {
      return if x == self.sorted[l] { min(i, h-l) } else { 0 };
    }
    let m = l+(h-l)/2;
    let mut ret = 0;
    let j =
      if i >= h-l { m-l }
      else { self.rnk[d][l+i] };
    if x <= self.sorted[m-1] {
      ret += self.rank_(d+1, l, m, x, j);
    }
    if self.sorted[m] <= x {
      ret += self.rank_(d+1, m, h, x, i-j);
    }
    ret
  }

  // 0 <= ll < hh <= n
  // 0 <= k < hh-ll
  pub fn kth(&self, ll: usize, hh: usize, k: usize) -> usize {
    self.kth_(0, 0, self.n, ll, hh, k)
  }

  fn kth_(&self, d: usize, l: usize, h: usize, ll: usize, hh: usize, k: usize) -> usize {
    if h-l == 1 {
      return self.sorted[l];
    }
    let m = l+(h-l)/2;
    let cl = self.rnk[d][ll];
    let ch =
      if hh == h { m-l }
      else { self.rnk[d][hh] };
    if k < ch-cl {
      self.kth_(d+1, l, m, l+cl, l+ch, k)
    } else {
      self.kth_(d+1, m, h, m+(ll-l-cl), m+(hh-l-ch), k-(ch-cl))
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use std::cmp::min;
  #[test]
  fn test() {
    let a = vec![3,1,3,6,2,0,2,2];
    let wt = UncompressedWaveletTree::new(a.clone());
    assert_eq!(wt.select(0, 0), 5);
    assert_eq!(wt.select(1, 0), 1);
    assert_eq!(wt.select(2, 0), 4);
    assert_eq!(wt.select(2, 1), 6);
    assert_eq!(wt.select(2, 2), 7);
    assert_eq!(wt.select(3, 0), 0);
    assert_eq!(wt.select(3, 1), 2);
    for i in 0..8 {
      for j in 0..20 {
        let mut c = 0;
        for k in 0..min(j, a.len()) {
          if a[k] == i { c += 1 }
        }
        assert_eq!(wt.rank(i, j), c);
      }
    }
    for i in 0..a.len() {
      let mut b = vec![];
      for j in i+1..a.len() {
        b.push(a[j-1]);
        b.sort();
        for k in 0..j-i {
          assert_eq!(wt.kth(i, j, k), b[k]);
        }
      }
    }
  }
}
