#![crate_type = "lib"]
#![feature(core)]
use std::u32;
use std::iter;
use std::intrinsics;

extern crate bit;
use bit::BitVec;

/// limit: n < 2**31

fn print_type_of<T>(_: &T) -> () {
  let type_name =
    unsafe {
      (*std::intrinsics::get_tydesc::<T>()).name
    };
  println!("{}", type_name);
}

fn identity_getter(x: u64) -> u64 { x }
fn not_getter(x: u64) -> u64 { ! x }

fn select_in_u16(mut x: u16, mut k: u32) -> u32 {
  while k > 0 {
    x &= x-1;
    k -= 1;
  }
  unsafe {
    intrinsics::cttz16(x) as u32
  }
}

fn select_in_u64(mut x: u64, mut k: u32) -> u32 {
  let mut c;
  unsafe {
    c = intrinsics::ctpop16(x as u16) as u32; if c > k { return select_in_u16(x as u16, k) + 0 }
    x >>= 16; k -= c;
    c = intrinsics::ctpop16(x as u16) as u32; if c > k { return select_in_u16(x as u16, k) + 16 }
    x >>= 16; k -= c;
    c = intrinsics::ctpop16(x as u16) as u32; if c > k { return select_in_u16(x as u16, k) + 32 }
    x >>= 16; k -= c;
  }
  select_in_u16(x as u16, k) + 48
}

pub static BLOCK_LEN: u32 = 1024;
pub static SUBBLOCK_LEN: u32 = 32;
pub static MAX_IN_BLOCK_OFFSET: u32 = 65536;

macro_rules! implement_darray {
  ($name:ident, $getter:ident) => {
    #[derive(Clone)]
    pub struct $name {
      block: Vec<i32>,
      subblock: Vec<u16>,
      overflow: Vec<u32>,
      num: usize,
    }

    impl $name {
      pub fn new(bit_vec: &BitVec) -> $name {
        let mut pos = vec![];
        let mut ret = $name {
          block: vec![],
          subblock: vec![],
          overflow: vec![],
          num: 0,
        };
        for (w, &word) in bit_vec.storage.iter().enumerate() {
          let mut i = w*64;
          let mut word = $getter(word);
          while word > 0 {
            let l = unsafe{ intrinsics::cttz64(word) as usize }; // may be 64
            i += l;
            word >>= l;
            if i >= bit_vec.len() { break; }
            pos.push(i as u32);
            if pos.len() == BLOCK_LEN as usize {
              ret.flush_cur_block(&mut pos);
            }
            i += 1;
            word >>= 1;
            ret.num += 1;
          }
        }
        if ! pos.is_empty() {
          ret.flush_cur_block(&mut pos);
        }
        ret
      }

      pub fn select(&self, bit_vec: &BitVec, idx: u32) -> u32 {
        let bi = idx / BLOCK_LEN;
        let bpos = self.block[bi as usize];
        if bpos < 0 {
          return self.overflow[(! bpos as usize) + (idx % BLOCK_LEN) as usize];
        }
        let sbi = idx / SUBBLOCK_LEN;
        let p = bpos as u32 + self.subblock[sbi as usize] as u32;
        let mut rem = idx % SUBBLOCK_LEN;
        if rem == 0 {
          return p;
        }
        let mut wi = p/64;
        let mut word = $getter(bit_vec.storage[wi as usize]) & ((-1) << p%64);
        loop {
          let pop = unsafe{ intrinsics::ctpop64(word) } as u32;
          if rem < pop {
            break;
          }
          rem -= pop;
          wi += 1;
          word = $getter(bit_vec.storage[wi as usize]);
        }
        64 * wi + select_in_u64(word, rem) as u32
      }

      pub fn space_consumption(&self) -> usize {
        self.block.len()*4 + self.subblock.len()*2 + self.overflow.len()*4
      }

      fn flush_cur_block(&mut self, pos: &mut Vec<u32>) {
        if pos[pos.len()-1]-pos[0] < MAX_IN_BLOCK_OFFSET {
          self.block.push(pos[0] as i32);
          for i in iter::range_step(0, pos.len(), SUBBLOCK_LEN as usize) {
            self.subblock.push((pos[i]-pos[0]) as u16);
          }
        } else {
          self.block.push(! self.overflow.len() as i32);
          for &p in pos.iter() {
            self.overflow.push(p);
          }
          for _ in iter::range_step(0, pos.len(), SUBBLOCK_LEN as usize) {
            self.subblock.push(-1);
          }
        }
        pos.clear();
      }
    }
  }
}

implement_darray!{DArray1, identity_getter}
implement_darray!{DArray0, not_getter}

#[cfg(test)]
mod tests {
  use super::bit::BitVec;

  #[test]
  fn test() {
    let mut bs = BitVec::from_elem(70000, false);
    // block 0
    for &i in &[1,2] {
      bs.set(i, true);
    }
    for i in 4..1026 {
      bs.set(i, true);
    }
    // block 1
    for i in 0..1023 {
      bs.set(i+1028, true);
    }
    bs.set(1028+super::MAX_IN_BLOCK_OFFSET as usize, true);

    let da1 = super::DArray1::new(&bs);
    println!("space: {} bytes", da1.space_consumption());

    assert_eq!(da1.select(&bs, 0), 1);
    assert_eq!(da1.select(&bs, 1), 2);
    assert_eq!(da1.select(&bs, 2), 4);
    assert_eq!(da1.select(&bs, 3), 5);
    assert_eq!(da1.select(&bs, 1024), 1028);
    assert_eq!(da1.select(&bs, 2047), 1028+super::MAX_IN_BLOCK_OFFSET);

    let da0 = super::DArray0::new(&bs);
    println!("space: {} bytes", da0.space_consumption());

    assert_eq!(da0.select(&bs, 0), 0);
    assert_eq!(da0.select(&bs, 1), 3);
  }
}
