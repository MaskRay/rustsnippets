fn lower_bound<T: Ord>(a: &[T], x: T) -> usize {
  let mut l = 0;
  let mut h = a.len();
  while l < h {
    let m = l+(h-l)/2;
    if a[m] < x {
      l = m+1;
    } else {
      h = m;
    }
  }
  l
}

fn upper_bound<T: Ord>(a: &[T], x: T) -> usize {
  let mut l = 0;
  let mut h = a.len();
  while l < h {
    let m = l+(h-l)/2;
    if a[m] <= x {
      l = m+1;
    } else {
      h = m;
    }
  }
  l
}
