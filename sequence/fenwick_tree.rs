use std::num::Int;
use std::iter::*;

pub struct FenwickTree<T: Int> {
  n: usize,
  a: Vec<T>,
}

impl<T: Int> FenwickTree<T> {
  fn new(n: usize) -> FenwickTree<T> {
    FenwickTree {
      n: n,
      a: repeat(Int::zero()).take(n).collect(),
    }
  }

  fn from_vec(mut vec: Vec<T>) -> FenwickTree<T> {
    let n = vec.len();
    let mut ret = FenwickTree::new(n);
    for i in 0..n {
      let j = i|i+1;
      if j < n {
        let t = vec[j]+vec[i];
        vec[j] = t; // TODO https://github.com/rust-lang/rust/pull/23171
      }
      ret.a[i] = vec[i];
    }
    ret
  }

  fn add(&mut self, mut i: usize, x: T) {
    while i < self.n {
      let t = self.a[i]+x;
      self.a[i] = t; // TODO
      i |= i+1;
    }
  }
  fn prefix_sum(&self, mut i: usize) -> T {
    let mut sum: T = Int::zero();
    while i > 0 {
      sum = sum+self.a[i-1]; // TODO
      i &= i-1;
    }
    sum
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn test() {
    let mut a = vec![3,1,4,2,2,3];
    let mut f: FenwickTree<i32> = FenwickTree::from_vec(a.clone());
    let mut s = 0;
    for i in 0..a.len()+1 {
      assert_eq!(f.prefix_sum(i), s);
      if i < a.len() {
        s += a[i];
      }
    }

    a[2] += 1;
    a[3] += 2;
    a[5] += 3;
    f.add(2, 1);
    f.add(3, 2);
    f.add(5, 3);
    s = 0;
    for i in 0..a.len()+1 {
      assert_eq!(f.prefix_sum(i), s);
      if i < a.len() {
        s += a[i];
      }
    }
  }
}
